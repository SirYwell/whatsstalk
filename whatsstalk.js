var $chatbody;
var $chatstatus;
var $last;
var $name;
//click on body -> restart
$("body").click(function() {
    init();
});

//adds a h2 to the top left corner
$("body").append("<h2 id='stalk-status'>unbekannt</h2>")

//enable message
console.log("WhatsStalk is enabled now.");

//start
init();
run();

//runs the script for its first time
function init() {
    $chatbody = $("header.pane-chat-header").children(".chat-body");
    $chatstatus = $chatbody.children(".chat-status").children("span");
    $last = $chatstatus.text();
    if($last.length <= 2) {
        $last = "unbekannt";
    }
    $name = $chatbody.children(".chat-main").children("h2").children("span").text();
    console.log("Currently stalked user: " + $name + "; status: " + $last);
}

//reads updates from web.whatsapp.com
function update() {
    $chatbody = $("header.pane-chat-header").children(".chat-body");
    $chatstatus = $chatbody.children(".chat-status").children("span");
    if(!$chatstatus.text().match($last)) {
        if($chatstatus.text().length < 2) {
            var date = new Date();
            $("#stalk-status").text("zuletzt online " + date.getHours() + ":" + date.getMinutes());
            console.log($name + " geht offline um " + date.getHours() + ":" + date.getMinutes());
        }
        else {
            console.log($name + ": zuletzt online " + date.getHours() + ":" + date.getMinutes());
        }
        $last = $chatstatus.text();
    }
}

//runs an update every 2 seconds
function run() {
    setInterval(function() {
        update();
    }, 2000);
}